// обратботчик событий реагирует на какое-то событие(клик, нажатие клавиши, перемещение по странице)
//и запускает заданную функцию по проишествию события
let inputedValue = document.getElementById("input");
let invalidValue = document.getElementById("invalid-value")
let value = document.getElementById("value");


inputedValue.addEventListener("focusin", () => inputedValue.classList.add('focused'));
inputedValue.addEventListener("focusout", () => inputedValue.classList.remove('focused'));

inputedValue.onblur = function () {
    if (inputedValue.value.includes('-')) {
        inputedValue.classList.add("invalid-input");
        invalidValue.classList.add('invalid');
        invalidValue.innerHTML = 'Пожалуйста, введите правильную цену.'
    } else {
        inputedValue.classList.add('valid');
        inputedValue.classList.add('valid-input');
        value.innerHTML = `Текущая цена: ${inputedValue.value}`;
        value.append(createRemoveBtn());
        invalidValue.remove();
    }
}
;



function createRemoveBtn() {
    let newSpan = document.createElement("span");
    newSpan.innerHTML = "X";
    newSpan.className = "remove-btn";
    newSpan.addEventListener("click", function (event) {
        event.target.closest(".value").remove();
        inputedValue.value = "";
    });
    return newSpan;
}


